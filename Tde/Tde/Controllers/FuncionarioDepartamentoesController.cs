﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tde.Dados;
using Tde.Models;

namespace Tde.Controllers
{
    [Route("api/funcionariodeparatamento")]
    [ApiController]
    public class FuncionarioDepartamentoesController : ControllerBase
    {
        private readonly TdeContext _context;

        public FuncionarioDepartamentoesController(TdeContext context)
        {
            _context = context;
        }

        // GET: api/FuncionarioDepartamentoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FuncionarioDepartamento>>> GetFuncionarioDepartamentos()
        {
            return await _context.FuncionarioDepartamentos.ToListAsync();
        }

        // GET: api/FuncionarioDepartamentoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FuncionarioDepartamento>> GetFuncionarioDepartamento(int id)
        {
            var funcionarioDepartamento = await _context.FuncionarioDepartamentos.FindAsync(id);

            if (funcionarioDepartamento == null)
            {
                return NotFound();
            }

            return funcionarioDepartamento;
        }

        // PUT: api/FuncionarioDepartamentoes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFuncionarioDepartamento(int id, FuncionarioDepartamento funcionarioDepartamento)
        {
            if (id != funcionarioDepartamento.FuncionarioId)
            {
                return BadRequest();
            }

            _context.Entry(funcionarioDepartamento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FuncionarioDepartamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FuncionarioDepartamentoes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<FuncionarioDepartamento>> PostFuncionarioDepartamento(FuncionarioDepartamento funcionarioDepartamento)
        {
            _context.FuncionarioDepartamentos.Add(funcionarioDepartamento);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FuncionarioDepartamentoExists(funcionarioDepartamento.FuncionarioId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetFuncionarioDepartamento", new { id = funcionarioDepartamento.FuncionarioId }, funcionarioDepartamento);
        }

        // DELETE: api/FuncionarioDepartamentoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<FuncionarioDepartamento>> DeleteFuncionarioDepartamento(int id)
        {
            var funcionarioDepartamento = await _context.FuncionarioDepartamentos.FindAsync(id);
            if (funcionarioDepartamento == null)
            {
                return NotFound();
            }

            _context.FuncionarioDepartamentos.Remove(funcionarioDepartamento);
            await _context.SaveChangesAsync();

            return funcionarioDepartamento;
        }

        private bool FuncionarioDepartamentoExists(int id)
        {
            return _context.FuncionarioDepartamentos.Any(e => e.FuncionarioId == id);
        }
    }
}
