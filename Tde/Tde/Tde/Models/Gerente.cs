﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tde.Models
{
    public class Gerente
    {
        public int Id{ get; set; }
        public DateTime DataDeInicio { get; set; }
        public string Nome { get; set; }

        public Departamento departamento { get; set; }
    }
}
